<?php
/**
 * Prints a line in iCal format:
 * - max 75 octets per line (not including line ending)
 * - line ending = \r\n
 * - line continuation = \r\n\t
 *
 * @param string $line
 *   The line to pint in the iCal format)
 */
function printICalLine($line) {
  /**
   * Maximum length in octets that lines in iCal format should have.
   *
   * @var int $maxICalLineLen
   */
  $maxICalLineLen = 75;
  /*
   * Line ending that lines in iCal must have
   *
   * @var string $eol;
   */
  $eol = "\r\n";
  /*
   * Line continuation that split lines in iCal must have.
   *
   * @var string $line_cont;
   */
  $line_cont = "\r\n\t";

  // Do a multi byte safe split of $line in chunks of max. $maxICalLineLen
  // octets.
  while (strlen($line) > $maxICalLineLen) {
    // Get a sub line that (in octets) is shorter than $maxICalLineLen.
    $line1 = drupal_substr($line, 0, $maxICalLineLen);
    while (strlen($line1) > $maxICalLineLen) {
      $line1 = drupal_substr($line1, 0, -1);
    }

    // Print that sub line ($line1) and continue with remainder.
    print $line1 . $line_cont;
    $line = substr($line, strlen($line1));
  }

  // $line is now shorter (in octets) than $maxICalLineLen: print it.
  print $line . $eol;
}

/**
 * Returns a date in the iCal date format.
 *
 * @param \Datetime $date
 *
 * @return string
 *   A date according tot he iCal date format
 */
function getICalDate(DateTime $date = null) {
  $iCalFormat = 'Ymd';
  return $date ? $date->format($iCalFormat) : gmdate($iCalFormat);
}

/**
 * Returns a time stamp in the iCal date-time format.
 *
 * @param \Datetime $date
 *
 * @return string
 *   A time stamp according tot he iCal date-time format
 */
function getICalDateTime(DateTime $date = null) {
  $iCalFormat = 'Ymd\THis\Z';
  return $date ? $date->format($iCalFormat) : gmdate($iCalFormat);
}
