<?php

/**
 * @file
 * Default theme implementation to display an iCal event.
 *
 * Available variables:
 * @var string $uid  A unique identifier for the event.
 * @var string $start  The start date of the event (yyyymmdd).
 * @var string $end  The end date of the event (yyyymmdd).
 * @var string $summary  A summary of the event (free format).
 * @var string $dtstamp  A string to use for the DTSTAMP property.
 *
 * If you are editing this file, remember that:
 * - In order to be compliant with the iCal spec, you better use the
 *   printICalLine() function defined in availability_calendar_ical_util.php
 *   (also see comments over there).
 *   @see http://tools.ietf.org/html/rfc5545#section-3.1
 * - Do not escape output: in an iCal feed there's no need to escape it as we
 *   are not in an html context. If this theme is called in an html context, the
 *   calling function should know so and is responsible for escaping..
 *
 * @ingroup themeable
 */
require_once('availability_calendar_ical_util.php');

printICalLine('BEGIN:VEVENT');
printICalLine('UID:' . $uid);
printICalLine('DTSTART;VALUE=DATE:' . $start);
printICalLine('DTEND;VALUE=DATE:' . $end);
printICalLine('SUMMARY:' . $summary);
printICalLine('DTSTAMP:' . $dtstamp);
printICalLine('END:VEVENT');
