<?php

/**
 * @file
 * Default theme implementation to display an iCal calendar.
 *
 * Available variables:
 * @var string $title  The name of the calendar.
 * @var string $prodId  The product name of the exporting system.
 * @var string $langcode  The ISO 3166 2 character language code defining the
 *   language used in the feed.
 * @var string $method  The action to perform for the event (PUBLISH or CANCEL).
 * @var string $dtstamp  A string to use for the DTSTAMP property.
 * @var \stdClass[] $events  An array of unavailable periods.
 *
 * If you are editing this file, remember that:
 * - In order to be compliant with the iCal spec, you better use the
 *   printICalLine() function defined in availability_calendar_ical_util.php
 *   (also see comments over there).
 *   @see http://tools.ietf.org/html/rfc5545#section-3.1
 * - Do not escape output: in an iCal feed there's no need to escape it as we
 *   are not in an html context. If this theme is called in an html context, the
 *   calling function should know so and is responsible for escaping..
 *
 * @ingroup themeable
 */
require_once('availability_calendar_ical_util.php');

printICalLine('BEGIN:VCALENDAR');
printICalLine('VERSION:2.0');
printICalLine('CALSCALE:GREGORIAN');
printICalLine('METHOD:' . $method);
if (!empty($title)):
  printICalLine('X-WR-CALNAME;VALUE=TEXT:' . $title);
endif;
printICalLine("PRODID:-//$prodId//$langcode");

foreach($events as $event):
  /** @noinspection PhpUnhandledExceptionInspection */
  print theme('availability_calendar_ical_vevent', array(
    'start' => getICalDate($event->start),
    'end' => getICalDate($event->end),
    'uid' => isset($event->uid) ? $event->uid : '',
    'summary' => isset($event->summary) ? $event->summary : '',
    'title' => $title,
    'langcode' => $langcode,
    'dtstamp' => $dtstamp,
  ));
endforeach;

printICalLine('END:VCALENDAR');
